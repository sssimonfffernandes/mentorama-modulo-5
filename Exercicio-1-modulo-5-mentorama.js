// array inicial
let numeros = [3, 12, 47, 4];
console.log(numeros);
//adicionar no início
numeros.unshift(22);
console.log(numeros);
//adicionar no fim
numeros.push(53);
console.log(numeros);
//apagar número do final
numeros.pop();
console.log(numeros);
//apagar número do início
numeros.shift()
console.log(numeros);
//inserir número no meio do array
numeros.splice(2, 0, 79);
console.log(numeros);
//novo array com os elementos originais
let numerosOriginais = numeros.filter(function(elemento){
  return elemento != 79;
});
console.log(numerosOriginais);


