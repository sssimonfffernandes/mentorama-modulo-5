let numeros = [10, 13, 25, 8, 73, 40, 3];
// Multiplicar cada elemento por 2
const arrDrobro = numeros.map((elemento) => elemento * 2);
console.log(arrDrobro);
// Mostrar os pares
const par = arrDrobro.filter((elemento) => elemento % 2 === 0);
console.log(par);
// Soma total
const soma = par.reduce((acc, cur) => acc + cur, 0);
console.log(soma);