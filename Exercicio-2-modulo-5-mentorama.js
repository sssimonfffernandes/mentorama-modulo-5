let numeros = [0, 13, 25, 8, 73, 40, 6];
// ordem crescente
function ordemCrescente (items){
  let tamanho = items.length;
  for(let i=0; i <= tamanho; i++){
    for(let j=0; j < (tamanho - i - 1); j++){
      if(items[j] > items[j+1]) {
                           var tmp = items[j]; 
                items[j] = items[j+1]; 
                items[j+1] = tmp; 
            }
    }
  }
}
ordemCrescente(numeros);
console.log(numeros);
// ordem decrescente
function ordemDecrescente (items){
  let tamanho = items.length;
  for(let i=0; i <= tamanho; i++){
    for(let j=0; j < (tamanho - i - 1); j++){
      if(items[j] < items[j+1]) {
                           var tmp = items[j]; 
                items[j] = items[j+1]; 
                items[j+1] = tmp; 
            }
    }
  }
}
ordemDecrescente(numeros);
console.log(numeros);

